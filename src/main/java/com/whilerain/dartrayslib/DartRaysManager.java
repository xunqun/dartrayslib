package com.whilerain.dartrayslib;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.RxBleConnection;
import com.polidea.rxandroidble2.RxBleDevice;
import com.polidea.rxandroidble2.scan.ScanSettings;
import com.whilerain.dartrayslib.command.BaseCommand;
import com.whilerain.dartrayslib.command.BaseRequest;
import com.whilerain.dartrayslib.command.GoHomeRequest;
import com.whilerain.dartrayslib.command.GoWorkRequest;
import com.whilerain.dartrayslib.command.NaviCancelRequest;
import com.whilerain.dartrayslib.command.SetupLanguageRequest;
import com.whilerain.dartrayslib.command.ShowNormalScreenCommand;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DartRaysManager {

    /**
     * The scanning time to find a device
     */
    private static final long SCAN_TIME = 5000;
    private static final long CMD_INTERVAL = 500;

    /**
     * MAC_ADDRESS for the target device
     */
    private String MAC_ADDRESS = "D1:13:EF:A0:00:0F";

    private static final String CHARACTERISTICS_WRITE = "0000fff2-0000-1000-8000-00805f9b34fb";
    private static final String CHARACTERISTICS_NOTIFY = "0000fff1-0000-1000-8000-00805f9b34fb";

    /**
     * Singleton instance
     */
    private static DartRaysManager instance;

    /**
     * TAG for logcat
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * RxBle instance
     */
    private RxBleClient rxBleClient;

    /**
     * All disposables that should be dispose at destroy
     */
    private CompositeDisposable disposables = new CompositeDisposable();

    /**
     * List of the registered listener
     */
    private List<Listener> listeners = new ArrayList();

    /**
     * Ble device object
     */
    private RxBleDevice device;

    /**
     * Disposable for scan,ing
     */
    private Disposable scanDisposable;

    /**
     * Timer for stop scanning
     */
    private CountDownTimer scanTimer;

    /**
     * The socket connection for I/O
     */
    private RxBleConnection bleConnection;

    /**
     * This map used to store the last update time of all kind of command
     */
    private HashMap<String, Long> cmdDelayMap = new HashMap<>();

    private HashMap<String, byte[]> cmdContentMap = new HashMap<>();


    private long currtime;

    private Thread exeCommandThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    if(!commandQueue.isEmpty()) {
                        BaseCommand command = commandQueue.take();
                        write(command.getTrasactionBytes());
                        Log.v(TAG, command.debugInfo());
                    }
                    Thread.currentThread().sleep(CMD_INTERVAL);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    });
    private BlockingQueue<BaseCommand> commandQueue = new LinkedBlockingDeque();

    /**
     * list of request
     */
    private enum Requests {
        GoHome(GoHomeRequest.class), GoWork(GoWorkRequest.class), NaviCancel(NaviCancelRequest.class), SetupLanguage(SetupLanguageRequest.class);
        Class<? extends BaseRequest> c;

        Requests(Class<? extends BaseRequest> c) {
            this.c = c;
        }

    }


    public interface Listener {
        void onDeviceFound();

        void onDeviceNotFound();

        void onDeviceConnected();

        void onRequestReceived(BaseRequest request);

        void onDeviceDisconnected();

        void onFailState(RxBleClient.State state);
    }

    private DartRaysManager() {

    }

    public synchronized static DartRaysManager getInstance() {
        if (instance == null) {
            instance = new DartRaysManager();
        }
        return instance;
    }

    public void setup(Context context, String macAddr) {
        if (macAddr == MAC_ADDRESS && isConnected()) {
            castDeviceConnected();
        } else {
            MAC_ADDRESS = macAddr;
            rxBleClient = RxBleClient.create(context);
        }
        try {
            exeCommandThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void scan() {
        device = null;
        scanDisposable = rxBleClient.scanBleDevices(new ScanSettings.Builder().build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(scanResult -> {
                    if (device == null && scanResult.getBleDevice().getMacAddress().equals(MAC_ADDRESS)) {
                        this.device = scanResult.getBleDevice();
                        if (scanTimer != null) {
                            scanTimer.cancel();
                            scanDisposable.dispose();
                        }
                        if(device != null) {
                            castDeviceFound();
                        }else{
                            castDeviceNotFound();
                        }
                    }
                }, throwable -> {
                });
        disposeAfterSecs(scanDisposable, 5);
    }

    public void connect() {
        if (device == null) {
            castDeviceNotFound();
            return;
        }
        if (isConnected()) {
            castDeviceConnected();
            return;
        }

        if (device.getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED) {

        }
        Disposable dispo = device.establishConnection(false)
                .subscribe(rxBleConnection -> {
                    bleConnection = rxBleConnection;
                    subscribeNotificationFromDevice(CHARACTERISTICS_NOTIFY);
                    castDeviceConnected();
                }, err -> {
                    Log.d(TAG, "error: " + err.getMessage());
                    disconnect();
                });

        disposables.add(dispo);
    }

    public boolean enqueue(BaseCommand command, boolean ignorable) {

        if (isConnected() && exeCommandThread.isAlive()) {
            try {
                currtime = System.currentTimeMillis();
                Long lasttime = cmdDelayMap.get(command.getClass().getSimpleName());
                Long deltaTime = (currtime - (lasttime == null ? 0 : lasttime));
                if ( deltaTime > 5000){
                    commandQueue.put(command);
                    cacheLastCommand(command);
                    return true;
                }

                if (ignorable && commandQueue.size() > 1) {
                    return false;
                }

                byte[] cachedContent = cmdContentMap.get(command.getClass().getSimpleName());
                if( cachedContent != null && Arrays.equals(cachedContent,command.getTrasactionBytes())){
                    return false;
                }

                commandQueue.put(command);
                cacheLastCommand(command);
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e){
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    private void cacheLastCommand(BaseCommand command) throws IOException {
        cmdDelayMap.put(command.getClass().getSimpleName(), currtime);
        cmdContentMap.put(command.getClass().getSimpleName(), command.getTrasactionBytes());
    }

    private void write(byte[] data) {
        if (device == null || bleConnection == null) {
            Observable.just(data).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(bytes -> {
                        castDeviceNotFound();
                    }, throwable -> Log.d(TAG, "write: fail"));

            return;
        }

        Disposable dispo = bleConnection.writeCharacteristic(UUID.fromString(CHARACTERISTICS_WRITE), data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> {

                }, t -> Log.d(TAG, "write: fail ---> " + t.getMessage()));
    }

    public void disconnect() {
        enqueue(new ShowNormalScreenCommand(), false);
        exeCommandThread.interrupt();
        bleConnection = null;
        if (!disposables.isDisposed()) {
            try {
                disposables.dispose();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        castDeviceDisconnected();
    }


    public boolean isConnected() {
        return bleConnection != null;
    }

    public void destory() {
        instance = null;
    }

    private void subscribeNotificationFromDevice(String uuid) {
        Disposable disposable = bleConnection.setupNotification(UUID.fromString(uuid))
                .flatMap((Function<Observable<byte[]>, Observable<byte[]>>) observable -> observable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> {

                    handleNotification(bytes);
                }, err -> {
                    Log.d(TAG, "error: " + err.getMessage());
                });
        disposables.add(disposable);
    }

    private void handleNotification(byte[] bytes) throws IllegalAccessException, InstantiationException {
        BaseRequest req;
        for (Requests q : Requests.values()) {
            req = q.c.newInstance();
            if (req.isMatch(bytes)) {
                castRequestReceived(req);
                break;
            }
        }
    }

    private void castFail(RxBleClient.State state) {
        Observable.fromIterable(listeners)
                .subscribe(l->l.onFailState(state), e->{});
    }

    private void castDeviceConnected() {
        Observable.fromIterable(listeners)
                .subscribe(l->l.onDeviceConnected(), e->{});
    }

    private void castDeviceDisconnected() {

        Observable.fromIterable(listeners)
                .subscribe(l->l.onDeviceDisconnected(), e->{});
    }

    private void castRequestReceived(BaseRequest q) {
        Observable.fromIterable(listeners)
                .subscribe(l->l.onRequestReceived(q), e->{});
    }

    private void disposeAfterSecs(Disposable dispos, int i) {
        scanTimer = new CountDownTimer(SCAN_TIME, SCAN_TIME) {

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                if (scanDisposable != null) dispos.dispose();
                if (device == null) castDeviceNotFound();
            }
        };
        scanTimer.start();

    }

    private void castDeviceFound() {
        Observable.fromIterable(listeners)
                .subscribe(l->l.onDeviceFound(), e->{});
    }

    private void castDeviceNotFound() {
        Observable.fromIterable(listeners)
                .subscribe(l->l.onDeviceNotFound(), e->{});

    }


    public void register(Listener l) {
        if(!listeners.contains(l)) {
            listeners.add(l);
        }
    }

    public void unregister(Listener l) {
        Iterator<Listener> iter = listeners.iterator();

        while (iter.hasNext()) {
            Listener listener = iter.next();

            if (l.equals(listener)) {
                iter.remove();
                break;
            }
        }
    }

}
