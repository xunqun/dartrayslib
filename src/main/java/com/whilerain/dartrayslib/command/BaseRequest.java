package com.whilerain.dartrayslib.command;

public abstract class BaseRequest extends BaseCommand {
    public boolean isMatch(byte[] data) {
        if(data[0] == HEADER1 &&
                data[1] == HEADER2 &&
                data[4] == command() &&
                data[5] == type() &&
                data[6] == function() ){
            return true;
        }
        return false;
    }
}
