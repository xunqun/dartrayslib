package com.whilerain.dartrayslib.command;

public class NaviLeftTimeCommand extends BaseCommand {
    int h, m;

    public NaviLeftTimeCommand(int h, int m) {
        this.h = h;
        this.m = m;
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x04;
    }

    @Override
    byte[] value() {
        return new byte[]{
                (byte) h,
                (byte) m};
    }

    @Override
    public String debugInfo() {
        return super.debugInfo() + "  " + h + ":" + m;
    }
}
