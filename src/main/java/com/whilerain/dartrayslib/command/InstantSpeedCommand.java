package com.whilerain.dartrayslib.command;

public class InstantSpeedCommand extends BaseCommand {

    private final int kmh;

    public InstantSpeedCommand(int kmh) {
        if(kmh < 0 ){
            this.kmh = 0;
        }else if( kmh > 999) {
            this.kmh = 999;
        }else{
            this.kmh = kmh;
        }
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x01;
    }

    @Override
    byte function() {
        return 0x01;
    }

    @Override
    byte[] value() {
        return new byte[]{(byte) ((kmh >> 8) & 0xFF), (byte) (kmh & 0xFF)};
    }


}
