package com.whilerain.dartrayslib.command;

public class IncomingCallCommand extends BaseCommand {

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x04;
    }

    @Override
    byte function() {
        return 0x07;
    }

    @Override
    byte[] value() {
        return new byte[]{0x00, 0x01};
    }


}
