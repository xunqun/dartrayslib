package com.whilerain.dartrayslib.command;

public class NaviTurnDistanceCommand extends BaseCommand {
    int meter;

    public NaviTurnDistanceCommand(int meter) {
        this.meter = meter;
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x01;
    }

    @Override
    byte[] value() {
        return new byte[]{(byte) ((meter >> 24) & 0xFF),
                (byte) ((meter >> 16) & 0xFF),
                (byte) ((meter >> 8) & 0xFF),
                (byte) (meter & 0xFF)};
    }

    @Override
    public String debugInfo() {
        return super.debugInfo() + "  " + meter;
    }


}
