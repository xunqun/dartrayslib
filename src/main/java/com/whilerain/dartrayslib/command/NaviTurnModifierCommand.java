package com.whilerain.dartrayslib.command;

public class NaviTurnModifierCommand extends BaseCommand {
    private Modifier modifier;

    public enum Modifier {
        straight(new byte[]{0, 0, 0, (byte) 0x01}),
        rightTurn(new byte[]{0, 0, 0, (byte) 0x02}),
        leftTurn(new byte[]{0, 0, 0, (byte) 0x04}),
        uturnRight(new byte[]{0, 0, 0, (byte) 0x08}),
        uturnLeft(new byte[]{0, 0, 0, (byte) 0x10}),
        rightSide(new byte[]{0, 0, 0, (byte) 0x20}),
        leftSide(new byte[]{0, 0, 0, (byte) 0x40}),
        merge(new byte[]{0, 0, 0, (byte) 0x80}),
        turnSlightRight(new byte[]{0, 0, (byte) 0x01, 0}),
        turnSlightLeft(new byte[]{0, 0, (byte) 0x02, 0}),
        destinationAtRight(new byte[]{0, 0, (byte) 0x04, 0}),
        destinationAtLeft(new byte[]{0, 0, (byte) 0x08, 0}),
        sharpRightTurn(new byte[]{0, 0, (byte) 0x10, 0}),
        sharpLeftTurn(new byte[]{0, 0, (byte) 0x20, 0}),
        rightFork(new byte[]{0, 0, (byte) 0x40, 0}),
        leftFork(new byte[]{0, 0, (byte) 0x80, 0}),
        rightExit(new byte[]{0, (byte) 0x01, 0, 0}),
        leftExit(new byte[]{0, (byte) 0x02, 0, 0}),
        rightForkStraight(new byte[]{(byte) 0x04, 0, 0, 0}),
        leftForkStraight(new byte[]{(byte) 0x08, 0, 0, 0}),
        rightRoundaboutExit1(new byte[]{0, (byte) 0x04, 0, 0}),
        rightRoundaboutExit2(new byte[]{0, (byte) 0x08, 0, 0}),
        rightRoundaboutExit3(new byte[]{0, (byte) 0x10, 0, 0}),
        rightRoundaboutExit4(new byte[]{0, (byte) 0x20, 0, 0}),
        leftRoundaboutExit1(new byte[]{0, (byte) 0x40, 0, 0}),
        leftRoundaboutExit2(new byte[]{0, (byte) 0x80, 0, 0}),
        leftRoundaboutExit3(new byte[]{(byte) 0x01, 0, 0, 0}),
        leftRoundaboutExit4(new byte[]{(byte) 0x02, 0, 0, 0});


        byte[] code;

        Modifier(byte[] c) {
            code = c;
        }
    }

    public NaviTurnModifierCommand(Modifier m) {
        modifier = m;
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x0E;
    }

    @Override
    byte[] value() {
        return modifier.code;
    }

    @Override
    public String debugInfo() {
        return super.debugInfo() + "  " + modifier;
    }
}
