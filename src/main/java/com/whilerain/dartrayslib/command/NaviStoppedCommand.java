package com.whilerain.dartrayslib.command;

public class NaviStoppedCommand extends BaseCommand {


    public NaviStoppedCommand() {

    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x02;
    }

    @Override
    byte[] value() {
        return new byte[]{
                (byte) 0,
                (byte) 2};
    }

}
