package com.whilerain.dartrayslib.command;

public class NaviComposedInfoCommand extends BaseCommand {
    int h, m;
    int kmh;
    int meter;

    public NaviComposedInfoCommand(int h, int m, int meter, int kmh) {
        this.h = h;
        this.m = m;

        if (kmh < 0) {
            this.kmh = 0;
        } else if (kmh > 999) {
            this.kmh = 999;
        } else {
            this.kmh = kmh;
        }

        this.meter = meter;
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x1b;
    }

    @Override
    byte[] value() {
        return new byte[]{
                (byte) ((kmh >> 8) & 0xFF),
                (byte) (kmh & 0xFF),
                (byte) ((meter >> 24) & 0xFF),
                (byte) ((meter >> 16) & 0xFF),
                (byte) ((meter >> 8) & 0xFF),
                (byte) (meter & 0xFF),
                (byte) h,
                (byte) m};
    }

    @Override
    public String debugInfo() {
        return super.debugInfo() + "  " + h + ":" + m + "|" + kmh + "|" + meter;
    }
}
