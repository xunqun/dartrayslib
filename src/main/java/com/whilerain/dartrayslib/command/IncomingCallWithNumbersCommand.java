package com.whilerain.dartrayslib.command;

import java.util.Arrays;


public class IncomingCallWithNumbersCommand extends BaseCommand {

    private String numString = "";
    int[] digits = new int[12];

    public IncomingCallWithNumbersCommand(String numString) {
        this.numString = numString;
        char temp;
        for (int i = 0; i < numString.length(); i++) {
            temp = numString.charAt(i);
            try {
                digits[i] = Integer.valueOf(temp);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x03;
    }

    @Override
    byte function() {
        return 0x03;
    }

    @Override
    byte[] value() {
        byte[] bytes = new byte[12];
        Arrays.fill(bytes, (byte)0x00);
        for(int i = 0; i<bytes.length;i++){
            bytes[i] = (byte) digits[i];
        }

        return bytes;
    }

}
