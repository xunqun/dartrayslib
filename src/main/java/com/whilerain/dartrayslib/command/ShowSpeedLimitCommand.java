package com.whilerain.dartrayslib.command;

import java.security.InvalidParameterException;

import static com.whilerain.dartrayslib.command.ShowSpeedLimitCommand.SupportedSpeedLimit.*;

public class ShowSpeedLimitCommand extends BaseCommand {
    private SupportedSpeedLimit limit = SupportedSpeedLimit.km50;

    enum SupportedSpeedLimit {
        km50(new byte[]{0x00, 0x01}),
        km60(new byte[]{0x00, 0x02}),
        km70(new byte[]{0x00, 0x03}),
        km80(new byte[]{0x00, 0x04}),
        km90(new byte[]{0x00, 0x05}),
        km100(new byte[]{0x00, 0x06}),
        km110(new byte[]{0x00, 0x07}),
        km120(new byte[]{0x00, 0x08}),
        km130(new byte[]{0x00, 0x09}),
        km140(new byte[]{0x00, 0x0a}),
        km150(new byte[]{0x00, 0x0b}),
        km160(new byte[]{0x00, 0x0c}),
        km170(new byte[]{0x00, 0x0d}),
        km180(new byte[]{0x00, 0x0e}),
        km190(new byte[]{0x00, 0x0f});


        byte[] code;

        SupportedSpeedLimit(byte[] code) {
            this.code = code;
        }
    }

    public ShowSpeedLimitCommand(int speed) throws InvalidParameterException{
        if(speed < 50 || speed > 190){
            throw new InvalidParameterException();
        }

        if(speed >= 50 && speed < 60){
            limit = km50;
        }else if (speed >= 60 && speed < 70){
            limit = km60;
        }else if(speed >= 70 && speed < 80){
            limit = km70;
        }else if(speed >= 80 && speed < 90){
            limit = km80;
        }else if(speed >= 90 && speed < 100){
            limit = km90;
        }else if(speed >= 100 && speed < 110){
            limit = km100;
        }else if(speed >= 110 && speed < 120){
            limit = km110;
        }else if(speed >= 120 && speed < 130){
            limit = km120;
        }else if(speed >= 130 && speed < 140){
            limit = km130;
        }else if(speed >= 140 && speed < 150){
            limit = km140;
        }else if(speed >= 150 && speed < 160){
            limit = km150;
        }else if(speed >= 160 && speed < 170){
            limit = km160;
        }else if(speed >= 170 && speed < 180){
            limit = km170;
        }else if(speed >= 180 && speed < 190){
            limit = km180;
        }else if(speed >= 190 && speed < 200){
            limit = km190;
        }
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x03;
    }

    /**
     * 0x01 for English
     * 0x02 for Traditional Chinese
     * 0x03 for Simplified Chinese
     *
     * @return
     */
    @Override
    byte[] value() {
        return limit.code;
    }


}
