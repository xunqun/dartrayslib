package com.whilerain.dartrayslib.command;

public class ShowNaviScreenCommand extends BaseCommand {

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x01;
    }

    @Override
    byte function() {
        return 0x0B;
    }

    @Override
    byte[] value() {
        return new byte[]{0x00, 0x00};
    }



}
