package com.whilerain.dartrayslib.command;

public class GoHomeRequest extends BaseRequest {

    @Override
    byte command() {
        return 0x30;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x0F;
    }

    @Override
    byte[] value() {
        return new byte[]{0x00};
    }

    @Override
    public String debugInfo() {
        return getClass().getSimpleName();
    }

}
