package com.whilerain.dartrayslib.command;

public class SetupLanguageCommand extends BaseCommand {
    private  SupportedLanguage language = SupportedLanguage.English;
    public enum SupportedLanguage{
        English(new byte[]{0x00, 0x01}),
        TraditionalChinese(new byte[]{0x00, 0x02}),
        SimplifiedChinese(new byte[]{0x00, 0x03});

        byte[] code;
        SupportedLanguage(byte[] code){
            this.code = code;
        }
    }

    public SetupLanguageCommand(SupportedLanguage lang){
        language = lang;
    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x01;
    }

    @Override
    byte function() {
        return 0x03;
    }

    /**
     * 0x01 for English
     * 0x02 for Traditional Chinese
     * 0x03 for Simplified Chinese
     * @return
     */
    @Override
    byte[] value() {
        return language.code;
    }


}
