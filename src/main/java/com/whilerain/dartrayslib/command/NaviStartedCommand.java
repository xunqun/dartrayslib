package com.whilerain.dartrayslib.command;

public class NaviStartedCommand extends BaseCommand {


    public NaviStartedCommand() {

    }

    @Override
    byte command() {
        return 0x10;
    }

    @Override
    byte type() {
        return 0x02;
    }

    @Override
    byte function() {
        return 0x02;
    }

    @Override
    byte[] value() {
        return new byte[]{
                (byte) 0,
                (byte) 1};
    }

}
