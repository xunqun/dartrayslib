package com.whilerain.dartrayslib.command;

import com.whilerain.dartrayslib.utils.Utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class BaseCommand {
    public final static byte HEADER1 = (byte) 0xAA;
    public final static byte HEADER2 = (byte) 0x55;
    public final static byte COMMAND_WRITE = (byte) 0x10;
    public final static byte COMMAND_QUERY = (byte) 0x20;
    public final static byte COMMAND_RECEIVE = (byte) 0x30;
    public final static byte TYPE_NORMAL = (byte) 0x10;
    public final static byte TYPE_NAVIGATION = (byte) 0x20;
    public final static byte TYPE_MESSAGE = (byte) 0x40;

    abstract byte command();

    abstract byte type();

    abstract byte function();

    abstract byte[] value();

    public String debugInfo(){
        try {
            return getClass().getSimpleName() + " " + Utility.bytesToHex(getTrasactionBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getClass().getSimpleName();
    }

    protected byte getCheckSum(byte[] array) {
        byte sum = 0;
        for (byte b : array) {
            sum += b;
        }
        return sum;
    }

    protected byte sumOfByteArray(byte[] value) {
        byte sum = 0;
        for (byte b : value) {
            sum += b;
        }
        return sum;
    }

    protected int getTotalLength() {
        return 8 + value().length;
    }

    public byte[] getTrasactionBytes() throws IOException {

        byte[] result = new byte[]{HEADER1,
                HEADER2,
                (byte) getTotalLength(),
                (byte) value().length,
                command(),
                type(),
                function()
        };

        result = concatenateByteArrays(result, value());
        result = concatenateByteArrays(result, new byte[]{getCheckSum(result)});

        return result;
    }

    public byte[] concatenateByteArrays(byte[] a, byte[] b) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        output.write(a);
        output.write(b);

        return output.toByteArray();
    }

}
